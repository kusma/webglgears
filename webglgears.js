var gl;

var program;
var gears;

var vertex_shader = `
attribute vec3 position;
attribute vec3 normal;

uniform mat4 ModelViewProjectionMatrix;
uniform mat4 NormalMatrix;
uniform vec3 LightSourceDirection;
uniform vec3 MaterialColor;

varying vec3 Color;

void main(void)
{
  // Transform the normal to eye coordinates
  vec3 N = normalize(vec3(NormalMatrix * vec4(normal, 0.0)));

  vec3 L = LightSourceDirection;

  float diffuse = max(dot(N, L), 0.0);
  float ambient = 0.2;

  // Multiply the diffuse value by the vertex color (which is fixed in this case)
  // and add the ambient term to get the actual color that we will use to draw
  // this vertex with
  Color = (ambient + diffuse) * MaterialColor;

  // Transform the position to clip coordinates
  gl_Position = ModelViewProjectionMatrix * vec4(position, 1.0);
}`;

var fragment_shader = `
precision mediump float;
varying vec3 Color;

void main(void)
{
  gl_FragColor = vec4(Color, 1.0);
}`;

function gears_init() {
  var v = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(v, vertex_shader);
  gl.compileShader(v);
  console.log("vertex shader info: " + gl.getShaderInfoLog(v));

  var f = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(f, fragment_shader);
  gl.compileShader(f);
  console.log("fragment shader info: " + gl.getShaderInfoLog(f));

  program = gl.createProgram();
  gl.attachShader(program, v);
  gl.attachShader(program, f);

  gl.bindAttribLocation(program, 0, "position");
  gl.bindAttribLocation(program, 1, "normal");

  gl.linkProgram(program);

  if (!gl.getProgramParameter(program, gl.LINK_STATUS))
    throw new Error(gl.getProgramInfoLog(program));

  gl.useProgram(program);

  program.ModelViewProjectionMatrix_location = gl.getUniformLocation(program, "ModelViewProjectionMatrix");
  program.NormalMatrix_location = gl.getUniformLocation(program, "NormalMatrix");
  program.MaterialColor_location = gl.getUniformLocation(program, "MaterialColor");

  var LightSourceDirection_location = gl.getUniformLocation(program, "LightSourceDirection");
  var lightSourceDirection = vec3.create();
  vec3.normalize(lightSourceDirection, [5.0, 5.0, 10.0])
  gl.uniform3fv(LightSourceDirection_location, lightSourceDirection);

  function create_gear(inner_radius, outer_radius, width, teeth, tooth_depth) {
    var r0 = inner_radius;
    var r1 = outer_radius - tooth_depth / 2.0;
    var r2 = outer_radius + tooth_depth / 2.0;
    var da = 2.0 * Math.PI / teeth / 4.0;

    var vertices = [];
    var last_vertex = null;

    function emit_vertex(v) {
      vertices.push(v[0][0], v[0][1], v[0][2]);
      vertices.push(v[1][0], v[1][1], v[1][2]);
    }

    function emit_strip(verts) {
      if (last_vertex != null) {
        emit_vertex(last_vertex);
        emit_vertex(verts[0]);
      }

      for (var i = 0; i < verts.length; ++i)
        emit_vertex(verts[i]);

      last_vertex = verts[verts.length - 1];
    }

    for (i = 0; i <= teeth; ++i) {
      var angle = i * 2.0 * Math.PI / teeth;
      s = Array.from({ length: 5 }, (v, i) => Math.sin(angle + da * i));
      c = Array.from({ length: 5 }, (v, i) => Math.cos(angle + da * i));

      v = [
        [r1 * c[0], r1 * s[0]],
        [r2 * c[1], r2 * s[1]],
        [r2 * c[2], r2 * s[2]],
        [r1 * c[3], r1 * s[3]],
        [r1 * c[4], r1 * s[4]],
        [r0 * c[0], r0 * s[0]],
        [r0 * c[4], r0 * s[4]],
      ]

      // front
      n = [0, 0, 1]
      emit_strip([[[v[1][0], v[1][1], width * 0.5], n],
                  [[v[2][0], v[2][1], width * 0.5], n],
                  [[v[0][0], v[0][1], width * 0.5], n],
                  [[v[3][0], v[3][1], width * 0.5], n],
                  [[v[5][0], v[5][1], width * 0.5], n],
                  [[v[4][0], v[4][1], width * 0.5], n],
                  [[v[6][0], v[6][1], width * 0.5], n]]);

      // back
      n = [0, 0, -1]
      emit_strip([[[v[1][0], v[1][1], -width * 0.5], n],
                  [[v[2][0], v[2][1], -width * 0.5], n],
                  [[v[0][0], v[0][1], -width * 0.5], n],
                  [[v[3][0], v[3][1], -width * 0.5], n],
                  [[v[5][0], v[5][1], -width * 0.5], n],
                  [[v[4][0], v[4][1], -width * 0.5], n],
                  [[v[6][0], v[6][1], -width * 0.5], n]]);

      // outer extrusion
      for (j = 0; j < 4; ++j) {
        n = [v[j+1][1] - v[j][1], -(v[j+1][0] - v[j][0]), 0]
        emit_strip([[[v[j][0],   v[j][1], +width * 0.5], n],
                    [[v[j][0],   v[j][1], -width * 0.5], n],
                    [[v[j+1][0], v[j+1][1], +width * 0.5], n],
                    [[v[j+1][0], v[j+1][1], -width * 0.5], n]]);
      }

      // inner extrusion
      ns = [[-c[0], -s[0], 0],
            [-c[4], -s[4], 0]]
      emit_strip([[[v[5][0], v[5][1], -width * 0.5], ns[0]],
                  [[v[5][0], v[5][1], +width * 0.5], ns[0]],
                  [[v[6][0], v[6][1], -width * 0.5], ns[1]],
                  [[v[6][0], v[6][1], +width * 0.5], ns[1]]]);


    }
    return vertices;
  }

  gears = [ create_gear(1.0, 4.0, 1.0, 20, 0.7),
            create_gear(0.5, 2.0, 2.0, 10, 0.7),
            create_gear(1.3, 2.0, 0.5, 10, 0.7) ];

  gears.forEach(gear => {
    gear.vbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, gear.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(gear), gl.STATIC_DRAW);
    gear.index_count = gear.length / 6;
  })

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.enable(gl.DEPTH_TEST);
  gl.enable(gl.CULL_FACE);
  gl.enableVertexAttribArray(0);
  gl.enableVertexAttribArray(1);
}

var mMatrix = mat4.create();
var vMatrix = mat4.create();
var pMatrix = mat4.create();

var mvMatrix = mat4.create();
var mvpMatrix = mat4.create();
var normal_matrix = mat4.create();

var tRot0 = -1.0;
var angle = 0.0;
var frames = 0;
var tRate0 = -1.0;

var view_rotx = 20.0;
var view_roty = 30.0;
var view_rotz = 0.0;
var animate = true;

function draw_gears() {
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  mat4.fromTranslation(vMatrix, [0.0, 0.0, -40.0]);
  mat4.rotate(vMatrix, vMatrix, view_rotx * Math.PI / 180, [1.0, 0.0, 0.0]);
  mat4.rotate(vMatrix, vMatrix, view_roty * Math.PI / 180, [0.0, 1.0, 0.0]);
  mat4.rotate(vMatrix, vMatrix, view_rotz * Math.PI / 180, [0.0, 0.0, 1.0]);

  var colors = [
    [0.8, 0.1, 0.0],
    [0.0, 0.8, 0.2],
    [0.2, 0.2, 1.0],
  ];

  var positions = [
    [-3.0, -2.0, 0.0],
    [ 3.1, -2.0, 0.0],
    [-3.1,  4.2, 0.0]
  ];

  var angles = [
    angle,
    -2 * angle - 9.0,
    -2 * angle - 25.0
  ];

  gears.forEach((gear, index) => {
    mat4.fromTranslation(mMatrix, positions[index]);
    mat4.rotate(mMatrix, mMatrix, angles[index] * Math.PI / 180, [0.0, 0.0, 1.0]);

    mat4.multiply(mvMatrix, vMatrix, mMatrix);
    mat4.multiply(mvpMatrix, pMatrix, mvMatrix);

    mat4.invert(normal_matrix, mvMatrix);
    mat4.transpose(normal_matrix, normal_matrix);

    gl.bindBuffer(gl.ARRAY_BUFFER, gear.vbo);
    gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 6 * 4, 0);
    gl.vertexAttribPointer(1, 3, gl.FLOAT, false, 6 * 4, 3 * 4);

    gl.uniformMatrix4fv(program.NormalMatrix_location, false, normal_matrix);
    gl.uniformMatrix4fv(program.ModelViewProjectionMatrix_location, false, mvpMatrix);
    gl.uniform3fv(program.MaterialColor_location, colors[index]);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, gear.index_count);
  })
}

function draw_frame() {
  t = performance.now() / 1000.0;

  if (tRot0 < 0.0)
     tRot0 = t;

  dt = t - tRot0;
  tRot0 = t;

  if (animate) {
     /* advance rotation for next frame */
     angle += 70.0 * dt;  /* 70 degrees per second */
     if (angle > 3600.0)
        angle -= 3600.0;
  }

  draw_gears();

  frames++;

  if (tRate0 < 0.0)
     tRate0 = t;
  if (t - tRate0 >= 5.0) {
     var seconds = t - tRate0;
     var fps = frames / seconds;
     console.log(`${frames} frames in ${seconds.toFixed(1)} seconds = ${fps.toFixed(3)} FPS`);
     tRate0 = t;
     frames = 0;
  }
}

(function() {
  var canvas = document.getElementById("webglgears");
  gl = canvas.getContext("webgl");

  function update() {
    var devicePixelRatio = window.devicePixelRatio || 1;
    var displayWidth  = Math.floor(canvas.clientWidth  * devicePixelRatio);
    var displayHeight = Math.floor(canvas.clientHeight * devicePixelRatio);
    if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
      canvas.width  = displayWidth;
      canvas.height = displayHeight;
      gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);

      h = gl.canvas.clientHeight / gl.canvas.clientWidth;
      mat4.frustum(pMatrix, -1.0, 1.0, -h, h, 5.0, 60.0);
    }

    draw_frame();
    window.requestAnimationFrame(update);
  }

  gears_init();

  document.body.addEventListener('keydown', function (evt) {
    switch (evt.code) {
    case 'ArrowLeft':
      view_roty += 5.0;
      break;
    case 'ArrowRight':
      view_roty -= 5.0;
      break;
    case 'ArrowUp':
      view_rotx += 5.0;
      break;
    case 'ArrowDown':
      view_rotx -= 5.0;
      break;
    case 'KeyA':
      animate = !animate;
      break;
    }
  })

  update();
})();
